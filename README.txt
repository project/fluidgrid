
-----------------------------------------------------------------------

  Fluid Grid System Framework for DRUPAL
  http://drupal.org/project/fluidgrid
  
  Hubert Florin
  couzin.hub@gmail.com
  Copyright 2009 Hubert Florin
   
-----------------------------------------------------------------------

You can read the HTML version of the documentation documentation here :


-----------------------------------------------------------------------

Introduction to Grid System:
----------------------------

The Fluid Grid System (FGS) for DRUPAL is a theme containing a css 
framework created by Hubert Florin, inspired by Bluetrip, 960, and 
other grid systems available on internet.

  - 960 : http://960.gs/
  - Bluetrip : http://bluetrip.org/
  - Blueprint : http://www.blueprintcss.org/

These systems have been created to improve the development process of
websites. When working within a framework, it is easier for web 
developers to create complicated and flexible layouts in a very short 
time. The grid frameworks creates a set of classes that you can use 
to organize your layout. With these systems, it becomes really easy
to achieve, and modify layouts without having to write any CSS. 


Installation
----------------------------

To use the FGS theme for Drupal, you must have a running version of 
Drupal. You can download the latest version here :

http://drupal.org/project/drupal

Note that FGS is not supported by Drupal 5, so you will need to 
upgrade your drupal installation to Drupal 6 or later in order to 
use it. You can download the latest version of the FGS

http://drupal.org/project/fluidgrid

Once downloaded, place the entire folder 'fluidgrid' in one of these
two location within your drupal installation :

  - /themes/
  - /sites/all/themes/

It is consider best practice to install themes that are not provided
with Drupal in the second location, as it makes the Drupal upgrading
process way easier, but both locations work the same.

Once the folder has been placed into the folder of your choice, open
your drupal site in a web-browser (preferably not Internet Explorer,
as it's now well known that Internet Explorer has been made by evil 
forces in order to destroy the internet), and navigate to the theme
page:

  - /admin/build/themes

Here you can see the list of all the themes that Drupal can use. 
You need to be logged in as an administrator to access this page.
You should see the Fluid Grid System theme listed. To activate it,
check the box 'Enabled' and select Fluid Grid as the default theme.
Then scroll down to the bottom of the page and click on "Save 
Configuration". When the page is reloaded, you should not be seeing 
Garland anymore but the white look of the Fluid Grid system. If you
still see Garland, don't panic, it might be because the administration
theme is still Garland. If you want to change this, navigate to:

  - /admin/settings/admin
  
There you can change the theme used for admin pages. If even after 
selecting another theme than Garland as the default theme you still 
see Garland, you can start to panic :) ... but my recomendation
would be to clear all caches, and if it remains ... re-install Drupal.


Configuration
----------------------------

Once the theme is activated, click on the 'configure' link of the 
theme, or go to:

  - /admin/build/themes/settings/fluidgrid

There you can select some standard settings, but if you scroll down, 
you will notice some extra settings specific to FGS.

-- Number of columns --

First, you can select the number of columns to use. By default, it is
set to 16, but for more flexibility, you can use as many columns as 
you'd like. A general rule is that more columns are for more 
complicated layouts, and no need of 24 columns if you only have a 
content and a sidebar, but really, it's up to you.

Depending on which amount of columns you want to use, the theme will
use a different page template. For example, if you want to use 24 
columns, the default page template of the theme will be 
'page-24.tpl.php' and the css used will be 'fluid-grid-24.css'. The 
combination of these two files will create a layout using 24 columns.
 
It is possible that you might need a very specific amount of columns
that is not here by default. You can create your own column setting,
but you will need to create the css file and alter the template.php
to accept your new system. You can also choose 'Set manually' and use
the standard page.tpl.php and style.css to create your grid.

Depending on the feedbacks from the user, I will probably add some
extra grids in the future.

-- Page Width --

You can also choose the width of the page. As the grid is created 
using percentages, it will adapt to the width of the page you are
choosing. Notice that if you enter a value, the layout will be 
a fixed width, and centered. To go back to a full fluid width, 
simply empty the page width input box

-- Side Margins --

If you wish to have a fluid layout but you don't want the page to 
reach the left and right sides of the browser's window, you can specify
a left and right margin to the page. Simply enter a value like '30px'
or even '5%', and your layout (only if the page width is not defined)
should have some left and right margins as defined.

-- Show / Hide Grid --

It is possible to view the grid you are working with by clicking on 
the (almost invisible) link at the bottom left of your browser's 
window : "Show/Hide Grid". The link is only available for the user #1 or 
for any role who have the permission 'administer site configuration'.

On the theme setting page you can choose if the grid should be displayed
by default, or if it should only be displayed if the link mentionned
above is clicked on. Check the box 'Show the grid by default' if you
wish to see the grid on each page reload, and uncheck it if you only 
want to see it when desired.

-- Block Edit Links --

One extra feature of the FGS is the edit link for block or menu items.
This feature is part of BASIC 2, the theme I used (and created) to 
build FGS. Basic 2 is a strip down version of Zen, and still my favorite
theme starter if you prefer not working with Grids. So this feature and
the next one are both originally from the Zen theme.

The block Edit Links, when checked, will display a link when hovering a 
block or a Drupal menu to edit the block or menu. It is very useful when
developing a new theme, but could be annoying when using the site. So 
I would recommend turning this off when the development process is over.

-- Theme registry --

Also from Basic 2 and originally from Zen, the theme registry setting
will clear the theme cache on every page reload. This is useful when
developing a new theme as some functions of the theme are cached by 
Drupal, like the one in the template.php. If you were to change these
functions or add a new one, or even remove one, you would have to visit 
the theme page in order to clear the cached version of the files and use
your new version. This is no longer necessary if the theme registry is 
checked (except if you modify the .info file, then you still have to visit
the theme page and re-submit it).

WARNING -> having the theme registry activated is very heavy on memory 
resources, and if this is fine when developing a new site, it might be a 
problem when the site goes live (it would cause your site to be very slow),
so remember to deactivate this setting once your site goes live.


Using the Grid system
----------------------------

Once your theme settings are good, you can start to build your page template.
There is several way to do this, some easy some not, so let's see which way 
better for you.

-- Method 1 -- Edit existing page template.

After you chose the number of columns to use, you can define which page 
template to modify to create your own layout. For example, if you chose











