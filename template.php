<?php // $Id$
    
// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('fluidgrid_rebuild_registry')) {
  drupal_rebuild_theme_registry();
}

// Adding the appropriate CSS file corresponding to the chosen grid
switch (theme_get_setting('fluidgrid_column_number')) {
    case 1:
        $grid = "12";
        break;
    case 0:
    case 2:
        $grid = "16";
        break;
    case 3:
        $grid = "20";
        break;
    case 4:
        $grid = "24";
        break;
}
$vars['css'] = drupal_add_css( path_to_theme() .'/css/fluid-grid-' . $grid . '.css', 'theme', 'all');


//
//	from ZEN // Override or insert PHPTemplate variables into the page templates.
//	
//	 This function creates the body classes that are relative to each page
//	
//	@param $vars
//	  A sequential array of variables to pass to the theme template.
//	@param $hook
//	  The name of the theme function being called ("page" in this case.)
//

function fluidgrid_preprocess_page(&$vars, $hook) {
  global $theme;

  // Don't display empty help from node_help().
  if ($vars['help'] == "<div class=\"help\"><p></p>\n</div>") {
    $vars['help'] = '';
  }
  
  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $body_classes = array($vars['body_classes']);
  if (user_access('administer blocks')) {
	  $body_classes[] = 'admin';
	}
  if (!$vars['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $body_classes[] = fluidgrid_id_safe('page-'. $path);
    $body_classes[] = fluidgrid_id_safe('section-'. $section);

    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-add'; // Add 'section-node-add'
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
      }
    }
  }
  
  // Theme Setting for the number of columns, directing
  // to the appropriate page template.
  
    if (!theme_get_setting('fluidgrid_pagetpl')) {
     switch (theme_get_setting('fluidgrid_column_number')) {
        case 1:
            $grid = "12";
            break;
        case 0:
        case 2:
            $grid = "16";
            break;
        case 3:
            $grid = "20";
            break;
        case 4:
            $grid = "24";
            break;
    }
    $vars['template_file'] = 'page-' . $grid;
  } else {
    $vars['template_file'] = theme_get_setting('fluidgrid_pagetpl');
  }
}

//
// Creating link to turn on/off the Grid
// in page template, use : echo switch_grid($link);
// to output the link
//

function switch_grid($link) {
  global $user;
  if ($user->uid == 1 || user_access('administer site configuration')) { 
    $output .= '<p id="switchgrid"><a class="off" href="#" title="turn the grid on or off">Show/Hide Grid</a></p>' ;
  }
  return $output;
}

//
// Page width and margin (set in the theme settings page)
//

function page_property($values) {
  if (theme_get_setting('fluidgrid_width') || theme_get_setting('fluidgrid_margin')) { 
    $output .= '<style type="text/css" media="screen">#page{' ;
    if (theme_get_setting('fluidgrid_width')) {
      $output .= "width:" . theme_get_setting('fluidgrid_width') . "px;" ;
    }
    if (theme_get_setting('fluidgrid_margin') && !theme_get_setting('fluidgrid_width')) {
      $output .= "margin:0 " . theme_get_setting('fluidgrid_margin') ;
    }
    $output .= '}</style>' ;
  
  }
  return $output;
}


//
// GRID MARKUP
// Creating the GRID (using only HTML and CSS)
// This will not be activated if you choose 'Set Manually' 
// in the theme settings. You can use this code to create your own grid
// But keep in mind that you will have to create your own css.
//
// In the page.tpl.php,  use echo fluidgrid($grid); to output the grid
//

function fluidgrid($grid) {
  
  // default 12 columns (all grids have at least 12)
  $output .= '<div id="grid" class="container ';
  if (theme_get_setting('fluidgrid_grid_display') == 1) {
    $output .= 'display';
  }
  $output .= '">
              <div class="col col-1 span-1"></div>
              <div class="col col-2 span-1"></div>
              <div class="col col-3 span-1"></div>
              <div class="col col-4 span-1"></div>
              <div class="col col-5 span-1"></div>
              <div class="col col-6 span-1"></div>
              <div class="col col-7 span-1"></div>
              <div class="col col-8 span-1"></div>
              <div class="col col-9 span-1"></div>
              <div class="col col-10 span-1"></div>
              <div class="col col-11 span-1"></div>
              <div class="col col-12 span-1"></div>';
  
  // Adding 4 columns for grid 16/20/24
  if (theme_get_setting('fluidgrid_column_number') == 0 
      || theme_get_setting('fluidgrid_column_number') == 2 
      || theme_get_setting('fluidgrid_column_number') == 3 
      || theme_get_setting('fluidgrid_column_number') == 4) {
    $output .= '<div class="col col-13 span-1"></div>
               <div class="col col-14 span-1"></div>
               <div class="col col-15 span-1"></div>
               <div class="col col-16 span-1"></div>';
  }
  
  // Adding 4 columns for grid 20/24
  if (theme_get_setting('fluidgrid_column_number') == 3
    || theme_get_setting('fluidgrid_column_number') == 4 ) {
    $output .=  '<div class="col col-17 span-1"></div>
                <div class="col col-18 span-1"></div>
                <div class="col col-19 span-1"></div>
                <div class="col col-20 span-1"></div>';
  }
  
  // Adding 4 columns for grid 24
  if (theme_get_setting('fluidgrid_column_number') == 4) {
    $output .=  '<div class="col col-21 span-1"></div>
                <div class="col col-22 span-1"></div>
                <div class="col col-23 span-1"></div>
                <div class="col col-24 span-1"></div>';
  }
  $output .= '</div>';
  return $output;
}


//
//	from ZEN // Override or insert PHPTemplate variables into the node templates.
//	
//	 This function creates the NODES classes, like 'node-unpublished' for nodes
//	 that are not published, or 'node-mine' for node posted by the connected user...
//	
//	@param $vars
//	  A sequential array of variables to pass to the theme template.
//	@param $hook
//	  The name of the theme function being called ("node" in this case.)
//

function fluidgrid_preprocess_node(&$vars, $hook) {
  global $user;

  // Special classes for nodes
  $node_classes = array();
  if ($vars['sticky']) {
    $node_classes[] = 'sticky';
  }
  if (!$vars['node']->status) {
    $node_classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['node']->uid && $vars['node']->uid == $user->uid) {
    // Node is authored by current user
    $node_classes[] = 'node-mine';
  }
  if ($vars['teaser']) {
    // Node is displayed as teaser
    $node_classes[] = 'node-teaser';
  }
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $node_classes[] = 'node-type-'. $vars['node']->type;
  $vars['node_classes'] = implode(' ', $node_classes); // Concatenate with spaces
}


//
// from ZEN // Override or insert PHPTemplate variables into the block templates.
//
//	This function create the EDIT LINKS for blocks and menus blocks.
//	When overing a block (except in IE6), some links appear to edit
//	or configure the block. You can then edit the block, and once you are
//	done, brought back to the first page.
//
// @param $vars
//   A sequential array of variables to pass to the theme template.
// @param $hook
//   The name of the theme function being called ("block" in this case.)
// 

function fluidgrid_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  if (theme_get_setting('fluidgrid_block_editing') && user_access('administer blocks')) {
    // Display 'edit block' for custom blocks
    if ($block->module == 'block') {
      $edit_links[] = l( t('edit block'), 'admin/build/block/configure/'. $block->module .'/'. $block->delta, array('title' => t('edit the content of this block'), 'class' => 'block-edit'), drupal_get_destination(), NULL, FALSE, TRUE);
    }
    // Display 'configure' for other blocks
    else {
      $edit_links[] = l(t('configure'), 'admin/build/block/configure/'. $block->module .'/'. $block->delta, array('title' => t('configure this block'), 'class' => 'block-config'), drupal_get_destination(), NULL, FALSE, TRUE);
    }

    // Display 'edit menu' for menu blocks
    if (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
      $edit_links[] = l(t('edit menu'), 'admin/build/menu', array('title' => t('edit the menu that defines this block'), 'class' => 'block-edit-menu'), drupal_get_destination(), NULL, FALSE, TRUE);
    }
    $vars['edit_links_array'] = $edit_links;
    $vars['edit_links'] = '<div class="edit">'. implode(' ', $edit_links) .'</div>';
  }
}


//
//  Create some custom classes for comments
//

function comment_classes($comment) {
  $node = node_load($comment->nid);
  global $user;
 
  $output .= ($comment->new) ? ' comment-new' : ''; 
  $output .=  ' '. $status .' '; 
  if ($node->name == $comment->name) {	
    $output .= 'node-author';
  }
  if ($user->name == $comment->name) {	
    $output .=  ' mine';
  }
  return $output;
}


// 	
// 	Customize the PRIMARY and SECONDARY LINKS, to allow the admin tabs to work on all browsers
// 	An implementation of theme_menu_item_link()
// 	
// 	@param $link
// 	  array The menu item to render.
// 	@return
// 	  string The rendered menu item.
// 	

function fluidgrid_menu_item_link($link) {
  if (empty($link['options'])) {
    $link['options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">'. check_plain($link['title']) .'</span>';
    $link['options']['html'] = TRUE;
  }

  if (empty($link['type'])) {
    $true = TRUE;
  }

  return l($link['title'], $link['href'], $link['options']);
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function fluidgrid_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary clear-block\">\n". $primary ."</ul>\n";
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= "<ul class=\"tabs secondary clear-block\">\n". $secondary ."</ul>\n";
  }

  return $output;
}

//	
//	Add custom classes to menu item
//	
	
function fluidgrid_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
#New line added to get unique classes for each menu item
  $css_class = fluidgrid_id_safe(str_replace(' ', '_', strip_tags($link)));
  return '<li class="'. $class . ' ' . $css_class . '">' . $link . $menu ."</li>\n";
}


//	
//	Converts a string to a suitable html ID attribute.
//	
//	 http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
//	 valid ID attribute in HTML. This function:
//	
//	- Ensure an ID starts with an alpha character by optionally adding an 'n'.
//	- Replaces any character except A-Z, numbers, and underscores with dashes.
//	- Converts entire string to lowercase.
//	
//	@param $string
//	  The string
//	@return
//	  The converted string
//	


function fluidgrid_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}